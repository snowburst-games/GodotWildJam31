// Audio data: contains references to all audio files in the project for convenience
// Contains method for loading required sounds - should load all sounds required for a stage at the start.
using System;
using System.Collections.Generic;
using Godot;

public static class AudioData
{

	public enum SoundBus {
		Voice,
		Effects,
		Music,
		Master,
		BadHeart
	}

	public static Dictionary<SoundBus, string> SoundBusStrings = new Dictionary<SoundBus, string>()
	{
		{SoundBus.Voice, "Voice"},
		{SoundBus.Effects, "Effects"},
		{SoundBus.Music, "Music"},
		{SoundBus.Master, "Master"}
	};

	public enum MainMenuSound {
		Music
	};

	public enum ButtonSound {
		Hover,
		Click
	};

	public enum WorldSound {
		Music
	};

	public enum SpellSound {
		WispBolt,
		Fireball2,
		Flamethrower,
		Incinerate,
		Firebolt,
		Fireball,
		Inferno1,
		Inferno,
		Fizzle
	};

	public enum PlayerSound {
		// BonePain,
		Ouch2,
		ScreamSkull,
		ScreamSkullCrunchBones,
		SkullMovement
	}

	public enum WispSound {
		SpiritDie,
		Ouch,
		Hurt1,
		Hurt2
	}

	public enum PortalSound
	{
		Portal1,
		Portal2
	}

	public enum TreasureSound
	{
		Coins,
		Crystal,
		Treasure,
		Scroll
	}

	public static Dictionary<TreasureSound, string> TreasureSoundPaths = new Dictionary<TreasureSound, string>()
	{
		{TreasureSound.Coins, "res://Props/Sound/Coins.wav"},
		{TreasureSound.Crystal, "res://Props/Sound/Crystal.wav"},
		{TreasureSound.Treasure, "res://Props/Sound/Treasure.wav"},
		{TreasureSound.Scroll, "res://Props/Sound/GetItem.wav"},
	};

	public static Dictionary<PortalSound, string> PortalSoundPaths = new Dictionary<PortalSound, string>()
	{
		{PortalSound.Portal1, "res://Props/Portal/Sound/Portal_v1.wav"},
		{PortalSound.Portal2, "res://Props/Portal/Sound/Portal_v2.wav"},
	};

	public static Dictionary<WispSound, string> WispSoundPaths = new Dictionary<WispSound, string>()
	{
		{WispSound.SpiritDie, "res://Actors/Wisp/Sound/SpriritDie.wav"},
		{WispSound.Hurt1, "res://Actors/Unit/Sound/Hurt1.wav"},
		{WispSound.Hurt2, "res://Actors/Unit/Sound/Hurt2.wav"},
		{WispSound.Ouch, "res://Actors/Unit/Sound/Ouch.wav"},
	};

	public static Dictionary<PlayerSound, string> PlayerSoundPaths = new Dictionary<PlayerSound, string>()
	{
		// {PlayerSound.BonePain, "res://Actors/Unit/Sound/BonePain.wav"},
		{PlayerSound.Ouch2, "res://Actors/Unit/Sound/Ouch2.wav"},
		{PlayerSound.ScreamSkull, "res://Actors/Unit/Sound/ScreamSkull.wav"},
		{PlayerSound.ScreamSkullCrunchBones, "res://Actors/Unit/Sound/ScreamSkullCrunchBones.wav"},
		{PlayerSound.SkullMovement, "res://Actors/Unit/Sound/SkullMovement.wav"}
	};

	public static Dictionary<SpellSound, string> SpellSoundPaths = new Dictionary<SpellSound, string>()
	{
		{SpellSound.WispBolt, "res://Actors/Spells/Firebolt/Fireball.wav"},
		{SpellSound.Fireball2, "res://Actors/Spells/Firebolt/Fireball2.wav"},
		{SpellSound.Flamethrower, "res://Actors/Spells/Incinerate/Fire/Flamethrower.wav"},
		{SpellSound.Incinerate, "res://Actors/Spells/Sound/Bolt.wav"},
		{SpellSound.Firebolt, "res://Actors/Spells/Sound/Fireball.wav"},
		{SpellSound.Fireball, "res://Actors/Spells/Sound/FireballBigger.wav"},
		{SpellSound.Inferno1, "res://Actors/Spells/Sound/Inferno_attack_spell.wav"},
		{SpellSound.Inferno, "res://Actors/Spells/Sound/Inferno_attack_spell2.wav"},
		{SpellSound.Fizzle, "res://Actors/Spells/Sound/Fizzle.wav"}
	};

	public static Dictionary<MainMenuSound, string> MainMenuSoundPaths = new Dictionary<MainMenuSound, string>()
	{
		{MainMenuSound.Music, "res://Stages/MainMenu/Music/Menu_tune_v0.1.ogg"}
	};

	public static Dictionary<WorldSound, string> WorldSoundPaths = new Dictionary<WorldSound, string>()
	{
		{WorldSound.Music, "res://Stages/World/Music/soul_searching_0.5.ogg"}
	};


	public static Dictionary<ButtonSound, string> ButtonSoundPaths = new Dictionary<ButtonSound, string>()
	{
		{ButtonSound.Click, "res://Interface/Buttons/BtnBase/ButtonClickScifi.wav"},
		{ButtonSound.Hover, "res://Interface/Buttons/BtnBase/ButtonHoverSciFi.wav"},
	};

	// Return a dictionary containing loaded sounds. Useful to do when loading/instancing a scene e.g. player.
	// Usage: Dictionary<AudioData.PlayerSounds, AudioStream> playerSounds = 
	//  AudioData.LoadedSounds<AudioData.PlayerSounds>(AudioData.PlayerSoundPaths);
	// If 2D positional sound is required then add a AudioStreamPlayer2D node as a child and:
	// AudioStreamPlayer2D soundPlayer = (AudioStreamPlayer2D) GetNode("SoundPlayer");
	// // Play sound
	// AudioHandler.PlaySound(soundPlayer, playerSounds[AudioData.PlayerSounds.Test], AudioData.SoundBus.Effects);
	// Change to AudioStreamPlayer for music/global sounds and AudioStreamPlayer3D if working in 3D
	public static Dictionary<T, AudioStream> LoadedSounds<T>(Dictionary<T, string> soundPathsDict)
	{
		Dictionary<T, AudioStream> loadedSoundsDict = new Dictionary<T, AudioStream>();
		foreach (T key in soundPathsDict.Keys)
		{
			loadedSoundsDict[key] = (AudioStream) GD.Load(soundPathsDict[key]);
		}
		return loadedSoundsDict;
	}


}
