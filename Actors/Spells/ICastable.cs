using Godot;
using System;

public interface ICastable
{
	void Start(float rotation);
	Spells.SpellType SpellType {get; set;}
}
