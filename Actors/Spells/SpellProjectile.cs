using Godot;
using System;
using System.Collections.Generic;

public class SpellProjectile : Area2D, ICastable
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	[Export]
	private float _speed = 600f;

	private AudioStreamPlayer2D _soundPlayer;
	private Dictionary<AudioData.SpellSound, AudioStream> _spellSounds = AudioData.LoadedSounds<AudioData.SpellSound>(AudioData.SpellSoundPaths);
	private float _distanceTravelled = 0;
	private AnimationPlayer _anim;

	[Export]
	private bool _AI = false;

	public Spells.SpellType SpellType {get; set;} = Spells.SpellType.Firebolt;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_anim = GetNode<AnimationPlayer>("Anim");
		_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
	}

	public void OnSpellBodyEntered(PhysicsBody2D body)
	{
		if (body is Unit unit)
		{
			if (unit.CurrentControlState is AIUnitControlState aI)
			{
				aI.OnBulletCollision();
				if (SpellType != Spells.SpellType.Firebolt)
				{
					return;
				}
				Die();
			}
			if (_AI)
			{
				if (unit.CurrentControlState is PlayerUnitControlState player)
				{
					player.OnBulletCollision();
					Die();
				}
			}
		}
	}

	public void Start(float rotation)
	{
		Rotation = rotation - (float)Math.PI/2f;

		switch (SpellType)
		{
			case Spells.SpellType.Firebolt:
				AudioHandler.PlaySound(_soundPlayer, _spellSounds[AudioData.SpellSound.Firebolt], AudioData.SoundBus.Effects);
				break;
			case Spells.SpellType.Fireball:
				AudioHandler.PlaySound(_soundPlayer, _spellSounds[AudioData.SpellSound.Fireball], AudioData.SoundBus.Effects);
				break;
			case Spells.SpellType.Incinerate:
				AudioHandler.PlaySound(_soundPlayer, _spellSounds[AudioData.SpellSound.Incinerate], AudioData.SoundBus.Effects);
				break;
			case Spells.SpellType.Inferno:
				AudioHandler.PlaySound(_soundPlayer, _spellSounds[AudioData.SpellSound.Inferno], AudioData.SoundBus.Effects);
				break;
			case Spells.SpellType.WispBolt:
				AudioHandler.PlaySound(_soundPlayer, _spellSounds[AudioData.SpellSound.WispBolt], AudioData.SoundBus.Effects);
				break;
		}
	}

	public async override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);
		if (SpellType == Spells.SpellType.Incinerate || SpellType == Spells.SpellType.Inferno)
		{
			// GetNode<AnimationPlayer>("Anim").Play("Start");
			PlayAnim("Start");
			await ToSignal(_anim, "animation_finished");
			Die();
			return;
		}
		Position += (Transform.x.Normalized() * _speed * delta);

		_distanceTravelled += _speed*delta;
		
		if (_distanceTravelled > 1500)
		{
			Die();
		}
	}

	public void PlayAnim(string animName)
	{
		if (_anim.IsPlaying())
		{
			return;
		}
		_anim.Play(animName);
	}

	public void Die()
	{
		// if (_soundPlayer.Playing)
		// {
		// 	await ToSignal(_soundPlayer, "finished");
		// }
		QueueFree();
	}

	//  // Called every frame. 'delta' is the elapsed time since the previous frame.
	//  public override void _Process(float delta)
	//  {
	//      
	

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
