using Godot;
using System;

public class BulletFire : Node2D
{
	public delegate void BulletFireEndedDelegate();
	public event BulletFireEndedDelegate BulletFireEnded;
	public delegate void BulletFireShapeChangedDelegate(bool big);
	public event BulletFireShapeChangedDelegate BulletFireShapeChanged;
	private AnimationPlayer _anim;
	private AnimationPlayer _animHit;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		GetNode<CPUParticles2D>("Inner").Emitting = false;
		GetNode<CPUParticles2D>("Outer").Emitting = false;
		_anim = GetNode<AnimationPlayer>("Anim");
		_animHit = GetNode<AnimationPlayer>("AnimHit");
	}

	public void OnHit()
	{
		_animHit.Play("Hit");
	}

	public void Start()
	{
		Visible = true;
		_anim.Play("Start");
	}

	public void End()
	{
		_anim.Play("End");
		// stop moving
		// disable collision shape?
	}
	private void OnAnimationFinished(String animName)
	{
		if (animName == "Start")
		{
			_anim.Play("End");
		}

		if (animName == "End")
		{
			BulletFireEnded?.Invoke();
			BulletFireEnded = null;
			BulletFireShapeChanged = null;
		}
	}

	private void OnShapeSmall()
	{
		BulletFireShapeChanged?.Invoke(false);
	}
	private void OnShapeBig()
	{
		BulletFireShapeChanged?.Invoke(true);
	}

}

