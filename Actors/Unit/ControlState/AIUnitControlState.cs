using Godot;
using System;
using System.Collections.Generic;

public class AIUnitControlState : UnitControlState
{
	public Steering Steering {get; set;}
	private Area2D _unitDetectArea;
	private CollisionShape2D _shape;
	public List<Vector2> CurrentPath = new List<Vector2>();
	
	public enum AIBehaviour { Wander , Chase, Shoot}
	public AIBehaviourState _currentAIBehaviourState;
	public Vector2 StartPosition {get; set;}
	public bool IsAvoiding {get; set;} = false;

	[Signal]
	public delegate void PathRequested(AIUnitControlState aIUnitControlState, Vector2 worldPosition);

	[Signal]
	public delegate void PathToPlayerRequested(AIUnitControlState aIUnitControlState);

	[Signal]
	public delegate void TryingToShoot(Vector2 origin);

	public AIUnitControlState(Unit unit)
	{
		this.Unit = unit;
		_unitDetectArea = this.Unit.GetNode<Area2D>("UnitDetectArea");
		_shape = this.Unit.GetNode<CollisionShape2D>("Shape");
		_unitDetectArea.Connect("body_entered", this, nameof(OnUnitDetectAreaEntered));
		_unitDetectArea.Connect("body_exited", this, nameof(OnUnitDetectAreaExited));
		StartPosition = this.Unit.Position;
		Steering = new Steering(maximumForce:75f, maximumSpeed:this.Unit.Speed, extents:((RectangleShape2D) _shape.Shape).Extents, separationFactor:3f);
		
		// Set default state here:
		SetAIBehaviourState(AIBehaviour.Wander);
	}
	public AIUnitControlState()
	{
		GD.Print("use constructor with unit argument");
		throw new InvalidOperationException();
	}

	public void SetSprite()
	{
		CPUParticles2D baseParticles = this.Unit.GetNode<CPUParticles2D>("Base");
		CPUParticles2D trailParticles = this.Unit.GetNode<CPUParticles2D>("Trail");
		switch (this.Unit.GemCarried)
		{
			case ManaGem.GemType.Normal:
				break;
			case ManaGem.GemType.Green:
				baseParticles.Texture = GD.Load<Texture>("res://Actors/Wisp/Art/Particles/Green.png");
				trailParticles.Texture = GD.Load<Texture>("res://Actors/Wisp/Art/Particles/Green.png");
				break;
			case ManaGem.GemType.Red:
				baseParticles.Texture = GD.Load<Texture>("res://Actors/Wisp/Art/Particles/Redish.png");
				trailParticles.Texture = GD.Load<Texture>("res://Actors/Wisp/Art/Particles/Redish.png");
				break;
		}
	}

	public void SetAIBehaviourState(AIBehaviour state)
	{
		if (_currentAIBehaviourState != null)
		{
			_currentAIBehaviourState.Die();
		}
		switch (state)
		{
			case AIBehaviour.Wander:
				_currentAIBehaviourState = new WanderAIBehaviourState(this);
				break;
			case AIBehaviour.Chase:
				_currentAIBehaviourState = new ChaseAIBehaviourState(this);
				break;
			case AIBehaviour.Shoot:
				_currentAIBehaviourState = new ShootAIBehaviourState(this);
				break;
			
		}
	}
	
	private void OnUnitDetectAreaEntered(PhysicsBody2D body)
	{
		if (! (body is StaticBody2D || body is KinematicBody2D || body is RigidBody2D))
		{
			return;
		}
		if (body is Unit unit)
		{
			if (!Steering.DetectedUnits.Contains(unit))
			{
				Steering.DetectedUnits.Add(unit);
			}
		}
		if (!Steering.DetectedPhysicsBodies.Contains(body))
		{
			Steering.DetectedPhysicsBodies.Add(body);
		}
	}


	private void OnUnitDetectAreaExited(PhysicsBody2D body)
	{
		if (! (body is StaticBody2D || body is KinematicBody2D || body is RigidBody2D))
		{
			return;
		}
		if (body is Unit unit)
		{
			if (Steering.DetectedUnits.Contains(unit))
			{
				Steering.DetectedUnits.Remove(unit);
			}
		}
		if (Steering.DetectedPhysicsBodies.Contains(body))
		{
			Steering.DetectedPhysicsBodies.Remove(body);
		}
	}

	private void RotateToTarget(Vector2 target, float delta)
	{
		if (Steering.MaximumForce == 75f) // at high enough steering force, rotate instantly
		{
			this.Unit.AnimRotation = TargetAnimRotation;
			return;
		}
		float lerpAngle = Mathf.LerpAngle(this.Unit.AnimRotation, TargetAnimRotation, Steering.MaximumForce/75f);
		this.Unit.AnimRotation = Mathf.PosMod(lerpAngle, 2*Mathf.Pi);
	}

	// THIS DOESN'T WORK!
	public bool IsCloseToDestination()
	{
		float areaExtents = (((RectangleShape2D) _shape.Shape).Extents.x * ((RectangleShape2D) _shape.Shape).Extents.y);
		if (CurrentPath.Count == 0)
		{
			return true;
		}
		if (this.Unit.Position.DistanceSquaredTo(CurrentPath[CurrentPath.Count-1]) < 7*areaExtents)
		{
			return true;
		}
		return false;
	}

	public override void Update(float delta)
	{
		base.Update(delta);
		_currentAIBehaviourState.Update(delta);
		if (CurrentPath.Count == 0)
		{
			this.Unit.CurrentVelocity = new Vector2(0,0);
			return;
		}
		// GD.Print(CurrentPath.Count);
		// incorproate the steering stuff into AI controls
		Steering.Update(this.Unit.Position);
		// GD.Print(Unit.CurrentVelocity);
		// if (Unit.CurrentVelocity.x == Mathf.NaN)
		// {
		// 	Unit.CurrentVelocity = new Vector2();
		// }
		float areaExtents = (((RectangleShape2D) _shape.Shape).Extents.x * ((RectangleShape2D) _shape.Shape).Extents.y);
		// GD.Print(areaExtents);
		// If we have a path, move towards it, otherwise just stay still
		if (CurrentPath.Count > 0)
		{
			if (CurrentPath.Count > 1)
			{
				if (this.Unit.Position.DistanceSquaredTo(CurrentPath[1]) < 7*areaExtents)
				{
					CurrentPath.RemoveAt(0);//(this.Unit.Position.DistanceSquaredTo(CurrentPath[0]));
				}
			}
					// GD.Print("is it this1?");
			RotateToTarget(CurrentPath[0], delta);
				
			if (CurrentPath.Count == 1)
			{
					// GD.Print("is it this2?");
				// if (this.Unit.Position.DistanceSquaredTo(CurrentPath[0]) < 7*areaExtents)
				// {
				// 	// this.Unit.CurrentVelocity = new Vector2(0,0);
				// 	CurrentPath.RemoveAt(0);
				// 	// Steering.IsAvoiding = false;
				// 	// GD.Print("test");
				// }
				// else
				{
					RotateAndMove(CurrentPath[0],true, delta);
				}
			}
			else if (CurrentPath.Count <= 2)
			{
					// GD.Print("is it thi3s?");
				RotateAndMove(CurrentPath[1],false, delta);
			}
			else
			{
					// GD.Print("is it this4?");
				RotateAndMove(CurrentPath[1],false, delta);
			}
		}

		OnCollision();
		// else
		// {
		// 	this.Unit.CurrentVelocity = new Vector2(0,0);
		// }

		
	}

	// Handle AI collisions
	private void OnCollision()
	{
		if (this.Unit.Died)
		{
			return;
		}
		foreach (KinematicCollision2D collision in this.Unit.CurrentCollisions)
		{
			if (collision.Collider is Unit unit)
			{
				if (unit.GetControlState() == Unit.ControlState.Player)
				{
					// GD.Print("Collided with enemy!");
					unit.CurrentControlState.OnEnemyCollision();
				}
			}
		}
	}

	public void RotateAndMove(Vector2 target, bool brake, float delta)
	{
		// Vector2 prevVelocity = this.Unit.CurrentVelocity;
		// MoveAndSlide( _steering.CalculateVelocity(target, brake));
		this.Unit.CurrentVelocity = Steering.CalculateVelocity(target, brake);

		SetTargetAnimRotation(target);
	}

	private bool _hitByBullet = false;

	public override void OnBulletCollision()
	{
		base.OnBulletCollision();

		if (_hitByBullet)
		{
			return;
		}
		_hitByBullet = true;
		this.Unit.Died = true;
		this.Unit.GetNode<CollisionShape2D>("Shape").SetDeferred("Disabled", true);
		this.Unit.PlaySoundVoice(this.Unit.WispSounds[GetRandomWispDeathSound()]);
		this.Unit.SetActionState(Unit.ActionState.Dying);
	}
	private Random _rand = new Random();

	public AudioData.WispSound GetRandomWispDeathSound()
	{
		AudioData.WispSound[] portalSounds = new AudioData.WispSound[4] {AudioData.WispSound.SpiritDie, AudioData.WispSound.Hurt1,AudioData.WispSound.Hurt2,AudioData.WispSound.Ouch};
		return portalSounds[_rand.Next(0, portalSounds.Length)];
	}

	public async override void OnDieAnimCompleted()
	{
		// base.OnDieAnimCompleted();
		EmitSignal(nameof(UnitControlState.FinishedDying), this.Unit);
		if (this.Unit.SoundPlayer.Playing)
			{
				await ToSignal(this.Unit.SoundPlayer, "finished");
			}
		this.Unit.QueueFree();
	}
}
