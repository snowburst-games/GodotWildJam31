using Godot;
using System;
using System.Collections.Generic;

public class UnitControlState : Reference
{
	[Signal]
	public delegate void FinishedDying();
	[Signal]
	public delegate void ManaChanged(float mana);
	
	public Dictionary<Spells.SpellType, float> SpellCosts {get; set;} = new Dictionary<Spells.SpellType, float>()
	{
		{Spells.SpellType.Firebolt, 2},
		{Spells.SpellType.Fireball, 10},
		{Spells.SpellType.Incinerate, 15},
		{Spells.SpellType.Inferno, 25}
	};
	private float _mana = 50f;
	public float Mana {
		get{
			return _mana;
		}
		set{
			_mana = value;
			EmitSignal(nameof(ManaChanged), value);
		}
	}
	public float MaxMana {get; set;} = 50f;
	public float ManaRegen {get; set;} = 1f;
	public Unit Unit {get; set;}
    public float TargetAnimRotation {get; set;} = 0;

	public Spells.SpellType SelectedSpell {get; set;} = Spells.SpellType.Firebolt;

	[Signal]
	public delegate void TriedCastingSpell(Spells.SpellType spell, Vector2 origin, float rotation);

	public bool TryCast {get; set;} = false;

    public virtual void Update(float delta)
    {

    }

    public void SetTargetAnimRotation(Vector2 target)
    {

		Vector2 direction = (target-this.Unit.Position).Normalized();
		// todo -refactor:		
		if (direction.x > 0.8f && Math.Abs(direction.y) < 0.2f)
		{
			TargetAnimRotation = Mathf.Pi/2;
		}
		else if (direction.x > 0.2f && direction.y > 0.2f)
		{
			TargetAnimRotation = 3*(Mathf.Pi/4);
		}
		else if (Math.Abs(direction.x) <0.2f && direction.y > 0.8f)
		{
			TargetAnimRotation = Mathf.Pi;
		}
		else if (direction.x < -0.2f && direction.y > 0.2f)
		{
			TargetAnimRotation = 5*(Mathf.Pi/4);
		}
		else if (direction.x < -0.8f && Math.Abs(direction.y) < 0.2f)
		{
			TargetAnimRotation = 6*(Mathf.Pi/4);
		}
		else if (direction.x < -0.2f && direction.y < -0.2f)
		{
			TargetAnimRotation = 7*(Mathf.Pi/4);
		}
		else if (Math.Abs(direction.x) < 0.2f && direction.y < -0.8f)
		{
			TargetAnimRotation = Mathf.Pi*2;
		}
		else if (direction.x > 0.2f && direction.y < -0.2f)
		{
			TargetAnimRotation = Mathf.Pi/4;
		}
		// else
		// {
		// 	GD.Print(direction);
		// }
    }

	public virtual void OnEnemyCollision()
	{

	}

	public virtual void OnBulletCollision()
	{
		
	}

	public virtual void OnDieAnimCompleted()
	{
		
	}
}
