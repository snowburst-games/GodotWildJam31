using Godot;
using System;

public class ShootAIBehaviourState : AIBehaviourState
{
	private Timer _shootTimer;
	private RandomNumberGenerator _rand = new RandomNumberGenerator();
	public ShootAIBehaviourState()
	{
		throw new InvalidOperationException();
	}    
	public ShootAIBehaviourState(AIUnitControlState unitControlState)
	{
		this.AIControl = unitControlState;
		_shootTimer = new Timer();
		_shootTimer.OneShot = true;
		unitControlState.Unit.AddChild(_shootTimer);
	}


	public override void Update(float delta)
	{

		if (_shootTimer.TimeLeft == 0)
		{
			AIControl.EmitSignal(nameof(AIUnitControlState.TryingToShoot),this.AIControl.Unit.Position);
			// GD.Print("tbalsd");
			_rand.Randomize();
			_shootTimer.WaitTime = _rand.RandfRange(0.1f, 1f);
			_shootTimer.Start();
		}

	}
	public override void Die()
	{
		base.Die();
		_shootTimer.QueueFree();
	}
}
