using Godot;
using System;

public class ChaseAIBehaviourState : AIBehaviourState
{

	private Timer _chaseTimer;
	private RandomNumberGenerator _rand = new RandomNumberGenerator();
	public ChaseAIBehaviourState()
	{
		throw new InvalidOperationException();
	}    
	public ChaseAIBehaviourState(AIUnitControlState unitControlState)
	{
		this.AIControl = unitControlState;
		_chaseTimer = new Timer();
		_chaseTimer.OneShot = true;
		unitControlState.Unit.AddChild(_chaseTimer);
	}


	public override void Update(float delta)
	{
		base.Update(delta);
		// get player position

		// GD.Print("test");
		if (AIControl.CurrentPath.Count <= 1 && _chaseTimer.TimeLeft == 0)
		{
			AIControl.EmitSignal(nameof(AIUnitControlState.PathToPlayerRequested),AIControl);
			// GD.Print("tbalsd");
			_rand.Randomize();
			_chaseTimer.WaitTime = _rand.RandfRange(1f, 2f);
			_chaseTimer.Start();
		}

		// if (AIControl.CurrentPath.Count <= 2 && AIControl.Steering.IsAvoiding)
		// {
		// 	if (AIControl.IsCloseToDestination())
		// 	{
		// 		AIControl.CurrentPath.Clear();
		// 	}
		// }
	}

	public override void Die()
	{
		base.Die();
		_chaseTimer.QueueFree();
	}
}
