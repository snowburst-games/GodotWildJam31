using Godot;
using System;
using System.Collections.Generic;

public class PlayerUnitControlState : UnitControlState
{

	private bool _collidedWithEnemy = false;

	public PlayerUnitControlState(Unit unit)
	{
		this.Unit = unit;
		this.Unit.Speed = 400f;
	}
    
	public PlayerUnitControlState()
	{
		GD.Print("use constructor with unit argument");
		throw new InvalidOperationException();
	}

    public override void Update(float delta)
    {
        base.Update(delta);

		bool up = Input.IsActionPressed("Up");
		bool down =  Input.IsActionPressed("Down");
		bool left =  Input.IsActionPressed("Left");
		bool right =  Input.IsActionPressed("Right");
		// bool cast = Input.IsActionPressed("Cast");
		

		SetTargetAnimRotation(this.Unit.Position + this.Unit.CurrentVelocity);
		this.Unit.CurrentVelocity = new Vector2( left ? -1 : (right ? 1 : 0), up ? -1 : (down ? 1 : 0)).Normalized();
		this.Unit.CurrentVelocity *= this.Unit.Speed;
		this.Unit.AnimRotation = TargetAnimRotation;
		OnCollision();
		if (CanAfford(SelectedSpell) && Input.IsActionJustPressed("Attack"))
		{
			TryCast = true;
		}
		Mana = Math.Min(MaxMana, Mana + ManaRegen * delta);
		// GD.Print(TryCast);
    }

	private bool CanAfford(Spells.SpellType spell)
	{
		if (Mana > SpellCosts[spell])
		{
			return true;
		}
		return false;
	}

	// Handle player collisions
	private void OnCollision()
	{
		foreach (KinematicCollision2D collision in this.Unit.CurrentCollisions)
		{
			if (collision.Collider is Unit unit)
			{
				if (unit.GetControlState() == Unit.ControlState.AI)
				{
					if (unit.Died)
					{
						return;
					}
					OnEnemyCollision();
				}
			}
		}
	}

    public override void OnBulletCollision()
    {
        base.OnBulletCollision();
		if (_collidedWithEnemy)
		{
			return;
		}
		_collidedWithEnemy = true;
		this.Unit.SetActionState(Unit.ActionState.Dying);
		GD.Print("Collided with enemy bullet!");
    }

    public override void OnEnemyCollision()
    {
        // base.OnEnemyCollision();
		if (_collidedWithEnemy)
		{
			return;
		}
		_collidedWithEnemy = true;
		this.Unit.PlaySoundVoice(this.Unit.PlayerSounds[GetRandomPlayerDeathSound()]);
		this.Unit.SetActionState(Unit.ActionState.Dying);
		GD.Print("Collided with enemy!");

    }

	private Random _rand = new Random();
	public AudioData.PlayerSound GetRandomPlayerDeathSound()
	{
		AudioData.PlayerSound[] portalSounds = new AudioData.PlayerSound[3] {AudioData.PlayerSound.Ouch2,AudioData.PlayerSound.ScreamSkullCrunchBones,AudioData.PlayerSound.ScreamSkull};
		return portalSounds[_rand.Next(0, portalSounds.Length)];
	}

    public override void OnDieAnimCompleted()
    {
        // base.OnDieAnimCompleted();
		EmitSignal(nameof(UnitControlState.FinishedDying));
    }
}
