using Godot;
using System;

public class CastingUnitActionState : UnitActionState
{
	public CastingUnitActionState(Unit unit)
	{
		this.Unit = unit;
		// GD.Print("entering idle action state");
		// Vector2 target = this.Unit.Position + this.Unit.CurrentVelocity;
		// Vector2 direction = (target-this.Unit.Position).Normalized();
		// GD.Print(this.Unit.AnimRotation);
		this.Unit.CurrentControlState.EmitSignal(nameof(UnitControlState.TriedCastingSpell), this.Unit.CurrentControlState.SelectedSpell,
			this.Unit.Position, this.Unit.AnimRotation);
		this.Unit.CurrentControlState.TryCast = false;
		this.Unit.SetActionAnim("Cast"); // player only so far
		switch (this.Unit.CurrentControlState.SelectedSpell)
		{
			case Spells.SpellType.Firebolt:
				this.Unit.GetNode<Timer>("AttackCooldown").WaitTime = 0.2f;
				break;
			case Spells.SpellType.Fireball:
				this.Unit.GetNode<Timer>("AttackCooldown").WaitTime = .7f;
				break;
			case Spells.SpellType.Incinerate:
				this.Unit.GetNode<Timer>("AttackCooldown").WaitTime = 1.5f;
				break;
			case Spells.SpellType.Inferno:
				this.Unit.GetNode<Timer>("AttackCooldown").WaitTime = 2.5f;
				break;
		}
		this.Unit.CurrentControlState.Mana -= this.Unit.CurrentControlState.SpellCosts[this.Unit.CurrentControlState.SelectedSpell];
		this.Unit.GetNode<Timer>("AttackCooldown").Start();
		// GD.Print(this.Unit.GetNode<Timer>("AttackCooldown").TimeLeft);
	}
	
	public CastingUnitActionState()
	{
		GD.Print("use constructor with unit argument");
		throw new InvalidOperationException();
		
	}

	public async override void Update(float delta)
	{
		base.Update(delta);

		await this.Unit.ToSignal(this.Unit.ActionAnim, "animation_finished");
		// we can wait for the cast animation to finish first
		this.Unit.SetActionState(Unit.ActionState.Idle);
		
	}
}
