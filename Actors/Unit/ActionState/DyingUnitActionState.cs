using Godot;
using System;

public class DyingUnitActionState: UnitActionState
{
	public DyingUnitActionState(Unit unit)
	{
		this.Unit = unit;
		this.Unit.SetActionAnim("Die");
	}
	
	public DyingUnitActionState()
	{
		GD.Print("use constructor with unit argument");
		throw new InvalidOperationException();
	}

	public override void Update(float delta)
	{
		base.Update(delta);

		// await this.Unit.ToSignal(this.Unit.ActionAnim, "animation_finished");


	}
}
