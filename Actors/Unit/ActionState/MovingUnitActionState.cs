using Godot;
using System;

public class MovingUnitActionState : UnitActionState
{
	
	public MovingUnitActionState(Unit unit)
	{
		this.Unit = unit;
		// GD.Print("entering moving action state");
	}
    
	public MovingUnitActionState()
	{
		GD.Print("use constructor with unit argument");
		throw new InvalidOperationException();
	}

	public override void Update(float delta)
    {
        base.Update(delta);
		this.Unit.SetActionAnim(this.Unit.WalkAnimationsByDirection[this.Unit.DirectionAnim]);
		this.Unit.PlayMovementSoundEffect();

		if (this.Unit.CurrentVelocity.LengthSquared() <= 1)
		{
			// if (this.Unit.CurrentControlState is PlayerUnitControlState)
			// GD.Print("idle");
			this.Unit.SetActionState(Unit.ActionState.Idle);
		}
		this.Unit.MoveAndSlide(this.Unit.CurrentVelocity);

		this.Unit.CurrentCollisions.Clear();
		for (int i = 0; i < this.Unit.GetSlideCount(); i++)
		{
			this.Unit.CurrentCollisions.Add(this.Unit.GetSlideCollision(i));
		}
		if (this.Unit.CurrentControlState.TryCast)
		{
			if (this.Unit.GetNode<Timer>("AttackCooldown").TimeLeft == 0)
			{
				this.Unit.SetActionState(Unit.ActionState.Casting);
			}
		}
    }

    public override void ExitState()
    {
        base.ExitState();
		this.Unit.SoundPlayerMovement.Stop();
    }
}
