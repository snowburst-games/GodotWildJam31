using Godot;
using System;
using System.Collections.Generic;

public class Unit : KinematicBody2D
{

	// in the right place..?
	public float MaxMana {get; set;} = 50f;
	public float Mana {get; set;} = 50f;
	public AudioStreamPlayer2D SoundPlayer {get; set;}
	public AudioStreamPlayer2D SoundPlayerMovement {get; set;}
	public Dictionary<AudioData.WispSound, AudioStream> WispSounds = AudioData.LoadedSounds<AudioData.WispSound>(AudioData.WispSoundPaths);
	public Dictionary<AudioData.PlayerSound, AudioStream> PlayerSounds = AudioData.LoadedSounds<AudioData.PlayerSound>(AudioData.PlayerSoundPaths);

	public float Speed {get; set;} = 200f;

	public bool Died {get; set;} = false;

	// public float Score {get; set;} = 1f;
	public ManaGem.GemType GemCarried {get; set;} = ManaGem.GemType.Normal;
	public List<KinematicCollision2D> CurrentCollisions {get; set;} = new List<KinematicCollision2D>();

	public AnimationPlayer ActionAnim {get; set;}
	private Sprite _sprite;
	private float _animRotation = 0;
	public float AnimRotation {
		get{
			return _animRotation;
		}
		set{
			_animRotation = value;
			if (DirectionsByRotation.ContainsKey((float)Math.Round(AnimRotation,1)))
			{
				DirectionAnim = DirectionsByRotation[(float)Math.Round(AnimRotation,1)];
			}			
		}
	}
	public enum ControlState { Player, AI }
	public UnitControlState CurrentControlState;

	public enum ActionState { Idle, Moving, MeleeAttacking, Dying, Casting }
	private UnitActionState _currentActionState;

	// TODO replace with AnimationTree
	public enum FacingDirection {Up,UpRight,Right,DownRight,Down,DownLeft,Left,UpLeft}
	public FacingDirection DirectionAnim {get; set;} = FacingDirection.Up;
	public Dictionary<float,Unit.FacingDirection> DirectionsByRotation {get; set;} = new Dictionary<float, Unit.FacingDirection>()
	{
		{1.6f, Unit.FacingDirection.Right},
		{2.4f, Unit.FacingDirection.DownRight},
		{3.1f, Unit.FacingDirection.Down},
		{3.9f, Unit.FacingDirection.DownLeft},
		{4.7f, Unit.FacingDirection.Left},
		{5.5f, Unit.FacingDirection.UpLeft},
		{6.3f, Unit.FacingDirection.Up},
		{0, Unit.FacingDirection.Up},
		{0.8f, Unit.FacingDirection.UpRight}
	};
	public Dictionary<Unit.FacingDirection, string> IdleAnimationsByDirection {get; set;} = new Dictionary<FacingDirection, string>()
	{
		{Unit.FacingDirection.Up, "IdleUp"},
		{Unit.FacingDirection.UpRight, "IdleUpRight"},
		{Unit.FacingDirection.Right, "IdleRight"},
		{Unit.FacingDirection.DownRight, "IdleDownRight"},
		{Unit.FacingDirection.Down, "IdleDown"},
		{Unit.FacingDirection.DownLeft, "IdleDownRight"},
		{Unit.FacingDirection.Left, "IdleRight"},
		{Unit.FacingDirection.UpLeft, "IdleUpRight"}
	};
	public Dictionary<Unit.FacingDirection, string> WalkAnimationsByDirection {get; set;} = new Dictionary<FacingDirection, string>()
	{
		{Unit.FacingDirection.Up, "WalkUp"},
		{Unit.FacingDirection.UpRight, "WalkUpRight"},
		{Unit.FacingDirection.Right, "WalkRight"},
		{Unit.FacingDirection.DownRight, "WalkDownRight"},
		{Unit.FacingDirection.Down, "WalkDown"},
		{Unit.FacingDirection.DownLeft, "WalkDownRight"},
		{Unit.FacingDirection.Left, "WalkRight"},
		{Unit.FacingDirection.UpLeft, "WalkUpRight"}
	};

	[Export]
	private ControlState _controlState = ControlState.AI;
	private ActionState _actionState = ActionState.Idle;

	public override void _Ready()
	{
		base._Ready();
		ActionAnim = GetNode<AnimationPlayer>("ActionAnim");
		_sprite = GetNode<Sprite>("Sprite");
		SoundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
		SoundPlayerMovement = GetNode<AudioStreamPlayer2D>("SoundPlayerMovement");
		SetControlState(_controlState);
		SetActionState(_actionState);
		
	}

	public void PlaySoundVoice(AudioStream stream)
	{
		if (SoundPlayer.Playing && SoundPlayer.Stream == stream)
		{
			return;
		}
		AudioHandler.PlaySound(SoundPlayer, stream, AudioData.SoundBus.Voice);
	}

	public void PlayMovementSoundEffect()
	{
		if (SoundPlayerMovement.Playing)
		{
			return;
		}
		if (! (CurrentControlState is PlayerUnitControlState))
		{
			return;
		}
		AudioHandler.PlaySound(SoundPlayerMovement, PlayerSounds[AudioData.PlayerSound.SkullMovement], AudioData.SoundBus.Effects);
	}

	public void SetControlState(ControlState state)
	{
		switch (state)
		{
			case ControlState.AI:
				CurrentControlState = new AIUnitControlState(this);
				break;
			case ControlState.Player:
				CurrentControlState = new PlayerUnitControlState(this);
				break;
		}
	}

	public void SetActionState(ActionState state)
	{
		_actionState = state;
		if (_currentActionState != null)
		{
			_currentActionState.ExitState();
		}
		switch (state)
		{
			case ActionState.Idle:
				_currentActionState = new IdleUnitActionState(this);
				break;
			case ActionState.Moving:
				_currentActionState = new MovingUnitActionState(this);
				break;
			case ActionState.MeleeAttacking:
				_currentActionState = new MeleeAttackingUnitActionState(this);
				break;	
			case ActionState.Dying:
				_currentActionState = new DyingUnitActionState(this);
				break;
			case ActionState.Casting:
				_currentActionState = new CastingUnitActionState(this);
				break;
		}
	}

	public ControlState GetControlState()
	{
		return _controlState;
	}


	// public void OnEnemyCollision()
	// {
	// 	OnEnemyCollision();
	// }

	public void SetActionAnim(string animation)
	{
		if (CurrentControlState is AIUnitControlState && animation != "Die")
		{
			if (CurrentVelocity.Normalized() != new Vector2(0,0))
			{
				GetNode<CPUParticles2D>("Trail").Direction = -CurrentVelocity.Normalized();
			}
			switch (_actionState)
			{
				case ActionState.Moving:
					GetNode<CPUParticles2D>("Trail").InitialVelocity = 20;
					break;
				case ActionState.Idle:
					GetNode<CPUParticles2D>("Trail").InitialVelocity = 10;
					break;
				default:
					GetNode<CPUParticles2D>("Trail").InitialVelocity = 10;
					break;
			}
			return;
		}
		if (ActionAnim.IsPlaying() && ActionAnim.CurrentAnimation == animation)
		{
			return;
		}
		_sprite.FlipH = (DirectionAnim == FacingDirection.Left || DirectionAnim == FacingDirection.DownLeft || DirectionAnim == FacingDirection.UpLeft);
		
		ActionAnim.Play(animation);
	}

	public Vector2 CurrentVelocity {get; set;}

	public override void _PhysicsProcess(float delta)
	{
		if (CurrentControlState == null || _currentActionState == null)
		{
			return;
		}
		CurrentControlState.Update(delta);
		_currentActionState.Update(delta);

		// temp
		
	}

	public void OnEnteredPortal(Vector2 pos)
	{
		Position = pos;
	}

	public void FinishDie()
	{

	}

	private void OnActionAnimAnimationFinished(string anim)
	{
		if (anim == "Die")
		{
			
			CurrentControlState.OnDieAnimCompleted();
		}
	}

}

