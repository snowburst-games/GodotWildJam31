using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
// using System.Threading.Tasks;

public class WorldNavigation : Navigation2D
{
	private NavigationPolygonInstance _navPolygonInstance;
	private Godot.Thread _navThread = new Thread();

	public override void _Ready()
	{
		_navPolygonInstance = GetNode<NavigationPolygonInstance>("NavigationPolygonInstance");
		// Godot.Thread navthread = new Thread();
		// navthread.Start(this, nameof(OnAIPathRequested), )

	}

	public void SingleUse(CollisionPolygon2D collisionPolygon)
	{
		List<Vector2> newPolygon = new List<Vector2>();
		NavigationPolygon polygon = _navPolygonInstance.Navpoly;
		Transform2D polygonTransform = collisionPolygon.GetGlobalTransform();
		
		Vector2[] polygonBlueprint = collisionPolygon.Polygon;
		foreach (Vector2 point in polygonBlueprint)
		{
			newPolygon.Add(polygonTransform.Xform(point));
		}
		polygon.AddOutline(newPolygon.ToArray());
		polygon.MakePolygonsFromOutlines();

		_navPolygonInstance.Navpoly = polygon;
	}

	public void Finalise()
	{
		_navPolygonInstance.Enabled = false;
		_navPolygonInstance.Enabled = true;
	}

	// public void ThreadedOnAIPathToPlayerRequested(AIUnitControlState aIUnitControlState, Unit player)
	// {
	// 	_navThread.Start(this, nameof(GetAIPathToPlayerThreaded), aIUnitControlState.Unit.Position, player, true);
	// }

	// public void GetAIPathToPlayerThreaded(AIUnitControlState aIUnitControlState, Unit player, bool simplify)
	// {
	// 	aIUnitControlState.CurrentPath = GetSimplePath(aIUnitControlState.Unit.Position, player.Position,true).ToList();
	// 	CallDeferred(nameof(EndAIPathToPlayerThreaded));
	// }

	// public void EndAIPathToPlayerThreaded()
	// {
	// 	_navThread.WaitToFinish();
	// }

	public void OnAIPathRequested(AIUnitControlState aIUnitControlState, Vector2 worldPos)
	{
		aIUnitControlState.CurrentPath = GetSimplePath(aIUnitControlState.Unit.Position, worldPos,false).ToList();
	}


	public void OnAIPathToPlayerRequested(AIUnitControlState aIUnitControlState, Unit player)
	{
		// GD.Print(playerPos);
		// ThreadedOnAIPathToPlayerRequested(aIUnitControlState, player);
		aIUnitControlState.CurrentPath = GetSimplePath(aIUnitControlState.Unit.Position, player.Position,true).ToList();
	}



	// testing
	// public List<Vector2> DrawPath {get; set;} = new List<Vector2>();

	// public override void _PhysicsProcess(float delta)
	// {
	// 	base._PhysicsProcess(delta);
		
	// 	Update();
	// }

	// public override void _Draw()
	// {
	// 	base._Draw();

	// 	foreach (Vector2 point in DrawPath)
	// 	{
	// 		DrawCircle(point, 5, new Color(1,0,0));
	// 	}
	// }
}
