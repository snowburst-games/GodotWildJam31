using Godot;
using System;

public class ManaBar : ProgressBar
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
	public void OnManaChanged(float mana)
	{
		Value = (mana/50)*100;
		GetNode<Label>("Label").Text = Math.Floor(mana) + "/50";
	}
}
