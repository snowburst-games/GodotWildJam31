using Godot;
using System;
using System.Collections.Generic;

public class UnitGenerator : YSort
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	private PackedScene _scnWisp;
	private Node _cntObstacles;
	private TileMap _tileMap;
	private Random _rand = new Random();

	private Timer _timerEnemy;
	private Unit _player;

	private float _currentSpeed = 150f;
	private float _maxSpeed = 1000f;
	private int _wispLimit = 300;
	private int _startingWisps = 100;

	
	// remember to adjust this when wisps die as well
	private List<Unit> _wisps = new List<Unit>();


	[Signal]
	public delegate void NewAIUnit(Unit unit);

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_scnWisp = GD.Load<PackedScene>("res://Actors/Wisp/Wisp.tscn");
		_timerEnemy = GetNode<Timer>("TimerEnemy");
		_player = GetNode<Unit>("Player");
	}

	public void OnWispDied(Unit wisp)
	{
		_wisps.Remove(wisp);
	}

	//  // Called every frame. 'delta' is the elapsed time since the previous frame.
	//  public override void _Process(float delta)
	//  {
	//      
	//  }
	// public override void _PhysicsProcess(float delta)
	// {
	//     base._PhysicsProcess(delta);
	// }
	
	public void Initialise(Node cntObstacles, TileMap tileMap)
	{
		_cntObstacles = cntObstacles;
		_tileMap = tileMap;
		_timerEnemy.Start();

		for (int i = 0; i < _startingWisps; i++)
		{
			AddWisp();
		}
	}

	private void OnTimerEnemyTimeout()
	{
		if (_wisps.Count > _wispLimit)
		{
			return;
		}
		AddWisp();
		_timerEnemy.WaitTime = (float)Math.Max(0.05, _timerEnemy.WaitTime- (1/Math.Pow(_timerEnemy.WaitTime,2)));
		_timerEnemy.Start();
	}

	private void AddWisp()
	{
		Unit wisp = (Unit) _scnWisp.Instance();
		wisp.Position = GetValidSpawnPoint();
		_wisps.Add(wisp);

		AddChild(wisp);
		AIUnitControlState aI = (AIUnitControlState) wisp.CurrentControlState;
		aI.SetAIBehaviourState(AIUnitControlState.AIBehaviour.Wander);
		if (_currentSpeed > 245)
		{
			if (_rand.Next(0,100) < 50) // after 75 wisps, 50% chance to become chasing wisp
			{
				aI.SetAIBehaviourState(AIUnitControlState.AIBehaviour.Chase);
				wisp.GemCarried = ManaGem.GemType.Green;
				aI.SetSprite();
			}
		}
		if (_currentSpeed > 265)
		{
			if (_rand.Next(0,100) < 25) // after 100 wisps, 25% chance to become shooty wisp
			{
				aI.SetAIBehaviourState(AIUnitControlState.AIBehaviour.Shoot);
				wisp.GemCarried = ManaGem.GemType.Red;
				aI.SetSprite();
			}
		}
		GD.Print("Wisp added with speed " + _currentSpeed);
		wisp.Speed = _currentSpeed;
		_currentSpeed = Math.Min(_currentSpeed + 1, _maxSpeed);
		EmitSignal(nameof(NewAIUnit), wisp);
	}

	private Vector2 GetValidSpawnPoint()
	{
		Vector2 pos = _tileMap.MapToWorld((Vector2)_tileMap.GetUsedCells()[_rand.Next(0,_tileMap.GetUsedCells().Count)]);
		if (_player.Position.DistanceTo(pos) < 300)
		{
			return GetValidSpawnPoint();
		}
		return pos;
	}
	private void OnBtnFireBoltPressed()
	{
		_player.CurrentControlState.SelectedSpell = Spells.SpellType.Firebolt;
	}

	private void OnBtnFireballPressed()
	{
		_player.CurrentControlState.SelectedSpell = Spells.SpellType.Fireball;
		// GD.Print("fireball");
	}


	private void OnBtnIncineratePressed()
	{
		_player.CurrentControlState.SelectedSpell = Spells.SpellType.Incinerate;
	}


	private void OnBtnInfernoPressed()
	{
		_player.CurrentControlState.SelectedSpell = Spells.SpellType.Inferno;
	}

}

