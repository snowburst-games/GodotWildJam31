// - Main menu
// - Intro text that fades as the game starts
// - Player character (only mana bar - dies on touched)
// - Wisp (code and art)
// - Wisp factory - generate wisps that are faster and more numerous as the game progresses
// - Player spells: fire bolt, fireball (aoe), incinerate (aoe from character), inferno (very big aoe from character). Select spells with 1 to 4 or by clicking the buttons, spacebar to cast.
// - Crystals (restore mana)
// - Score (from killing wisps)
// - Defeat -> leaderboard (integrate leaderboard)

using Godot;
using System;
using System.Collections.Generic;

public class StageWorld : Stage
{
	// private WorldUnitHandler _worldUnitHandler = new WorldUnitHandler();
	private AudioStreamPlayer _musicPlayer;
	private Dictionary<AudioData.WorldSound, AudioStream> _worldSounds = AudioData.LoadedSounds<AudioData.WorldSound>(AudioData.WorldSoundPaths);
	private ScoreManagerMono _scoreManagerMono;
	private WorldNavigation _worldNavigation;
	private YSort _obstacles;
	private YSort _portals;
	private Gems _gems;
	private Spells _spells;
	private Scrolls _scrolls;
	private UnitGenerator _unitGenerator;
	private TileMap _tileMap;
	private ManaBar _manaBar;
	private Random _rand = new Random();
	private PackedScene _scnScroll;
	private PackedScene _gemScn;
	private HBoxSpells _hBoxSpells;
	private PnlMenu _pnlMenu;
	private PnlSettings _pnlSettings;
	List<Spells.SpellType> _scrollsToGenerate = new List<Spells.SpellType> {
		Spells.SpellType.Fireball, Spells.SpellType.Inferno, Spells.SpellType.Incinerate
	};

	public override void _Ready()
	{
		base._Ready();
		_pnlMenu = GetNode<PnlMenu>("CanvasLayer/PnlMenu");
		_pnlMenu.Visible = false;
		_pnlSettings = GetNode<PnlSettings>("CanvasLayer/PnlSettings");
		_pnlSettings.Visible = false;
		_musicPlayer = GetNode<AudioStreamPlayer>("MusicPlayer");
		_scoreManagerMono = GetNode<ScoreManagerMono>("CanvasLayer/ScoreManagerMono");
		_scoreManagerMono.GetNode("score_manager/score_saver").Connect("finished", this, nameof(OnScoreSaverFinished));
		_worldNavigation = GetNode<WorldNavigation>("WorldNavigation");
		_hBoxSpells = GetNode<HBoxSpells>("CanvasLayer/PnlBottom/HBoxSpells");
		_gemScn = GD.Load<PackedScene>("res://Props/ManaGem/ManaGem.tscn");
		_scnScroll = GD.Load<PackedScene>("res://Props/Scrolls/Scroll.tscn");
		_manaBar = GetNode<ManaBar>("CanvasLayer/ManaBar");
		_obstacles = GetNode<YSort>("All/Obstacles");
		_portals = GetNode<YSort>("All/Portals");
		_spells = GetNode<Spells>("All/Spells");
		_scrolls = GetNode<Scrolls>("All/Scrolls");
		_gems = GetNode<Gems>("All/Gems");
		_tileMap = GetNode<TileMap>("TileMap");
		_unitGenerator = GetNode<UnitGenerator>("All/Units");
		_unitGenerator.Connect(nameof(UnitGenerator.NewAIUnit), this, nameof(OnNewAIUnit));
		_unitGenerator.Initialise(_obstacles, _tileMap);
		AudioHandler.PlaySound(_musicPlayer, _worldSounds[AudioData.WorldSound.Music], AudioData.SoundBus.Music);
		// temporary for testing -will be replaced by dynamic unit generation
		foreach (Node n in GetNode<YSort>("All/Units").GetChildren())
		{
			if (n is Unit unit)
			{
				// _worldUnitHandler.AddUnit(unit);
				// if (unit.CurrentControlState is AIUnitControlState aIUnitControlState)
				// {
				// 	aIUnitControlState.Connect(nameof(UnitControlState.FinishedDying), this, nameof(OnEnemyDied));
				// 	aIUnitControlState.Connect(nameof(AIUnitControlState.PathRequested), _worldNavigation, nameof(WorldNavigation.OnAIPathRequested));
				// }
				// this part will need to be kept:
				if (unit.CurrentControlState is PlayerUnitControlState playerUnitControlState)
				{
					playerUnitControlState.Connect(nameof(UnitControlState.FinishedDying), this, nameof(OnPlayerDied));
					playerUnitControlState.Connect(nameof(UnitControlState.TriedCastingSpell), _spells, nameof(Spells.OnSpellCast));
					playerUnitControlState.Connect(nameof(UnitControlState.ManaChanged), _manaBar, nameof(ManaBar.OnManaChanged));
				}
			}
		}
		foreach (Node n in _portals.GetChildren())
		{
			if (n is PortalManager portalManager)
			{
				portalManager.Connect(nameof(PortalManager.OnTeleport), GetNode<Unit>("All/Units/Player"), nameof(Unit.OnEnteredPortal));
			}
		}
		SetNavigation();
	}
	private bool _gameOver = false;
	public void OnPlayerDied()
	{
		_gameOver = _pnlMenu.GameOver = true;

		GD.Print("LAUNCH GAME OVER SEQUENCE");
		_scoreManagerMono.StartScoreSaver((int)_score, 10);
		// foreach (Button btn in _hBoxSpells.GetChildren())
		// {
		// 	btn.Disabled = true;
		// }
		// GetNode<Button>("CanvasLayer/PnlBottom/BtnMenu").Disabled = true;
		foreach (Node n in GetNode("CanvasLayer").GetChildren())
		{
			if (n is Control c)
			{
				c.Visible = false;
			}
		}
		// temp
		// GetTree().Paused = true;
	}

	public void OnScoreSaverFinished()
	{
		_scoreManagerMono.ShowAndRefreshLeaderboard(10);
	}

	private void OnBtnMainMenuPressed()
	{
		SceneManager.SimpleChangeScene(SceneData.Stage.MainMenu);
	}


	private void OnBtnRestartPressed()
	{
		SceneManager.SimpleChangeScene(SceneData.Stage.World);
	}

	private void OnBtnMenuPressed()
	{
		if (_gameOver)
		{
			return;
		}
		GetTree().Paused = _pnlMenu.Visible = true;
	}


	private void OnBtnResumePressed()
	{
		GetTree().Paused = _pnlMenu.Visible = false;
	}


	private void OnPnlSettingsFinalClosed()
	{
		OnBtnResumePressed();
	}


	private void OnBtnSettingsPressed()
	{
		_pnlSettings.Visible = true;
		_pnlMenu.Visible = false;
	}

	private void OnBtnQuitPressed()
	{
		OnBtnMainMenuPressed();
	}

	// public override void _Process(float delta)
	// {
	// 	if (Input.IsActionJustPressed("Pause"))
	// 	{
	// 		if (!_pnlMenu.Visible)
	// 		{
	// 			OnBtnMenuPressed();
	// 		}
	// 	}
	// }

	

	public void OnEnemyDied(Unit wisp)
	{
		if (_rand.Next(0,100) < 5 && _scrollsToGenerate.Count != 0) // 5% chance of spawning a scroll (if there exist), otherwise a gem
		{
			GenerateScroll(wisp.Position);
		}
		else
		{
			SpawnGem(wisp.Position, wisp.GemCarried);
		}
		_unitGenerator.OnWispDied(wisp);
	}

	public void SpawnGem(Vector2 pos, ManaGem.GemType gemType)
	{
		ManaGem manaGem = (ManaGem) _gemScn.Instance();
		manaGem.Position = pos;
		manaGem.RotationDegrees = _rand.Next(0,361);
		manaGem.SetGemType(gemType);
		manaGem.Connect(nameof(ManaGem.GemPickedUp), this, nameof(OnGemPickedUp));
		_gems.AddChild(manaGem);
	}

	private float _score = 0;

	public void OnGemPickedUp(float score)
	{
		_score += score;
		GetNode<Label>("CanvasLayer/LblScore").Text = "Soul Fragments: " + _score;
		// GD.Print("SCORE is ", _score);
	}

	public void GenerateScroll(Vector2 position)
	{
		Spells.SpellType _spellToContain = _scrollsToGenerate[_rand.Next(0,_scrollsToGenerate.Count)];
		_scrollsToGenerate.Remove(_spellToContain);

		Scroll scroll = (Scroll) _scnScroll.Instance();
		// scroll.Scale = new Vector2(0,0); // start 0 for anim
		scroll.Position = position;
		_scrolls.AddChild(scroll);
		scroll.Connect(nameof(Scroll.SpellLearned), _hBoxSpells, nameof(HBoxSpells.EnableSpell));
		scroll.SetContainedSpell(_spellToContain);
	}

	private void OnNewAIUnit(Unit unit)
	{
		if (unit.CurrentControlState is AIUnitControlState aIUnitControlState)
		{
			aIUnitControlState.Connect(nameof(AIUnitControlState.PathRequested), _worldNavigation, nameof(WorldNavigation.OnAIPathRequested));
			aIUnitControlState.Connect(nameof(AIUnitControlState.PathToPlayerRequested), _worldNavigation, nameof(WorldNavigation.OnAIPathToPlayerRequested), 
				new Godot.Collections.Array {GetNode<Unit>("All/Units/Player")});
			aIUnitControlState.Connect(nameof(UnitControlState.FinishedDying), this, nameof(OnEnemyDied));
			aIUnitControlState.Connect(nameof(AIUnitControlState.TryingToShoot), _spells, nameof(Spells.OnAISpellCast));
		}
	}

	private void SetNavigation()
	{
		foreach (Node n in _obstacles.GetChildren())
		{
			if (n is StaticBody2D body)
			{
				foreach (Node bodyChild in body.GetChildren())
				{
					if (bodyChild is CollisionPolygon2D poly)
					{
						_worldNavigation.SingleUse(poly);
					}
				}
			}
		}
		_worldNavigation.Finalise();
	}



	// TESTING

	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);

		// _worldNavigation.DrawPath.Clear();
		
		// foreach (Node n in GetNode<YSort>("All/Units").GetChildren())
		// {
		// 	if (n is Unit unit)
		// 	{
		// 		if (unit.CurrentControlState is AIUnitControlState aIUnitControlState)
		// 		{				
		// 			foreach (Vector2 point in aIUnitControlState.CurrentPath)
		// 			{
		// 				_worldNavigation.DrawPath.Add(point);
		// 			}
		// 		}
		// 	}
		// }
	}

}

