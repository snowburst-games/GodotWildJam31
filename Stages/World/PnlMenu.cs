using Godot;
using System;

public class PnlMenu : Panel
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	public bool GameOver {get; set;} = false;
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		
	}

	public override void _Process(float delta)
	{
		if (Input.IsActionJustPressed("Pause"))
		{
			if (GameOver)
			{
				return;
			}
				GetTree().Paused = Visible = !Visible;
			
		}
	}
}
