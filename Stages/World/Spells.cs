using Godot;
using System;
using System.Collections.Generic;

public class Spells : YSort
{
	public enum SpellType {Firebolt, Fireball, Incinerate, Inferno, WispBolt}

	private Dictionary<SpellType, PackedScene> _spellScenes;
	private RandomNumberGenerator _rand = new RandomNumberGenerator();

	public override void _Ready()
	{
		_spellScenes = new Dictionary<SpellType, PackedScene>()
		{
			{SpellType.Firebolt, GD.Load<PackedScene>("res://Actors/Spells/Firebolt/SpellFireBolt.tscn")},
			{SpellType.Fireball, GD.Load<PackedScene>("res://Actors/Spells/Fireball/SpellFireBall.tscn")},
			{SpellType.Incinerate, GD.Load<PackedScene>("res://Actors/Spells/Incinerate/SpellIncinerate.tscn")},
			{SpellType.Inferno, GD.Load<PackedScene>("res://Actors/Spells/Inferno/SpellInferno.tscn")},
			{SpellType.WispBolt, GD.Load<PackedScene>("res://Actors/Spells/WispBolt/SpellWispBolt.tscn")}
		};
	}

	public void OnSpellCast(SpellType spell, Vector2 origin, float rotation)
	{
		// spell = SpellType.Inferno;
		ICastable newSpell = (ICastable) _spellScenes[spell].Instance();
		if (newSpell is Node2D node2D)
		{
			node2D.Position = origin;
			AddChild(node2D);
		}
		newSpell.SpellType = spell;
		newSpell.Start(rotation);
	}

	public void OnAISpellCast(Vector2 origin)
	{

		ICastable newSpell = (ICastable) _spellScenes[SpellType.WispBolt].Instance();
		if (newSpell is Node2D node2D)
		{
			node2D.Position = origin;
			AddChild(node2D);
		}
		newSpell.SpellType = SpellType.WispBolt; // same behaviour so i couldnt be bothered to do another
		newSpell.Start(_rand.RandfRange(0, Mathf.Pi*2));
	}

}
