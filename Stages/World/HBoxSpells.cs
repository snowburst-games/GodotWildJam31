using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class HBoxSpells : HBoxContainer
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	private List<Spells.SpellType> _enabledSpells = new List<Spells.SpellType>();
	private Dictionary<Spells.SpellType, Button> _spellBtns;
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_spellBtns = new Dictionary<Spells.SpellType, Button>()
		{
			{Spells.SpellType.Firebolt, GetNode<Button>("BtnFireBolt")},
			{Spells.SpellType.Fireball, GetNode<Button>("BtnFireball")},
			{Spells.SpellType.Incinerate, GetNode<Button>("BtnIncinerate")},
			{Spells.SpellType.Inferno, GetNode<Button>("BtnInferno")}
		};
		_enabledSpells.Add(Spells.SpellType.Firebolt);
		_enabledSpells.Add(Spells.SpellType.Fireball);
		_enabledSpells.Add(Spells.SpellType.Incinerate);
		_enabledSpells.Add(Spells.SpellType.Inferno);
		foreach (Button btn in GetChildren())
		{
			btn.Connect("pressed", this, nameof(OnSpellBtnPressed), new Godot.Collections.Array() {btn});
			btn.Disabled = true;
		}
		OnSpellBtnPressed(_spellBtns[Spells.SpellType.Firebolt]);
	}

	private void OnSpellBtnPressed(Button selectedBtn)
	{
		foreach (Button btn in GetChildren())
		{
			Spells.SpellType spell = _spellBtns.FirstOrDefault(x => x.Value == btn).Key;
			if (_enabledSpells.Contains(spell))
			{
				btn.Disabled = false;
				btn.Modulate = new Color(1,1,1);
			}
		}
		selectedBtn.Disabled = true;
		selectedBtn.Modulate = new Color(1.5f, 1.5f, 1.5f);
	}

	private void OnSpellKeyPressed(Button selectedBtn)
	{
		selectedBtn.EmitSignal("pressed");
		OnSpellBtnPressed(selectedBtn);
	}
	public override void _Input(InputEvent @event)
	{
		base._Input(@event);
		if (Input.IsActionPressed("SelectFireBolt"))
		{
			if (_enabledSpells.Contains(Spells.SpellType.Firebolt))
			{
				OnSpellKeyPressed(_spellBtns[Spells.SpellType.Firebolt]);
			}
		}
		if (Input.IsActionPressed("SelectFireball"))
		{
			if (_enabledSpells.Contains(Spells.SpellType.Fireball))
			{
				OnSpellKeyPressed(_spellBtns[Spells.SpellType.Fireball]);
			}
		}
		if (Input.IsActionPressed("SelectIncinerate"))
		{
			if (_enabledSpells.Contains(Spells.SpellType.Incinerate))
			{
				OnSpellKeyPressed(_spellBtns[Spells.SpellType.Incinerate]);
			}
		}
		if (Input.IsActionPressed("SelectInferno"))
		{
			if (_enabledSpells.Contains(Spells.SpellType.Inferno))
			{
				OnSpellKeyPressed(_spellBtns[Spells.SpellType.Inferno]);
			}
		}
	}

	public void EnableSpell(Spells.SpellType spell)
	{
		if (!_enabledSpells.Contains(spell))
		{
			_enabledSpells.Add(spell);
			_spellBtns[spell].Disabled = false;
		}
	}
}
