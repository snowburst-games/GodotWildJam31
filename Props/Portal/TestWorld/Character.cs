using Godot;
using System;

public class Character : Godot.KinematicBody2D
{
	[Export] public int Speed = 200;
	public Vector2 velocity = new Vector2(); 
	
	//public int PortalID = 0;

	public void GetInput()
	{
		velocity = new Vector2();

		if (Input.IsActionPressed("ui_right"))
			velocity.x += 1;

		if (Input.IsActionPressed("ui_left"))
			velocity.x -= 1;

		if (Input.IsActionPressed("ui_down"))
			velocity.y += 1;

		if (Input.IsActionPressed("ui_up"))
			velocity.y -= 1;

		velocity = velocity.Normalized() * Speed;
	}
	
	public override void _Ready()
	{
		
	}

	public override void _PhysicsProcess(float delta)
	{
		GetInput();
		velocity = MoveAndSlide(velocity);
	}

	private void OnTeleport(Vector2 pos)
	{
		GD.Print(pos + "destination teleport position recieved");
		this.Position = pos;
	}


}
