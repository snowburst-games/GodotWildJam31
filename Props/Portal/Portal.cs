using Godot;
using System;
using System.Collections.Generic;

public class Portal : Area2D
{
  //  [Export]
  	private Random _rand = new Random();
	public int Id;
	public bool lockPortal = false;
	private AudioStreamPlayer2D _soundPlayer;
	private Dictionary<AudioData.PortalSound, AudioStream> _portalSounds = AudioData.LoadedSounds<AudioData.PortalSound>(AudioData.PortalSoundPaths);

	public override void _Ready()
	{
		base._Ready();

		_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
	}

	[Signal]
	public delegate void OnPortalActivated(int id);

	private AudioData.PortalSound GetRandomPortalSound()
	{
		AudioData.PortalSound[] portalSounds = new AudioData.PortalSound[2] {AudioData.PortalSound.Portal1, AudioData.PortalSound.Portal2};
		return portalSounds[_rand.Next(0, portalSounds.Length)];
	}
	
	private void OnPortalBodyEntered(KinematicBody2D body)
	{
		if (body is KinematicBody2D && lockPortal == false)
		{
			int PortalID = this.Id;
			AudioHandler.PlaySound(_soundPlayer, _portalSounds[GetRandomPortalSound()], AudioData.SoundBus.Effects);
			EmitSignal(nameof(OnPortalActivated), PortalID);
		}
		return;
	}
}
