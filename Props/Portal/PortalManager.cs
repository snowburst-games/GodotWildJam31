using Godot;
using System;

public class PortalManager : Node2D
{
	int _portalEntered;
	public Portal Portal;
	Timer Timer;
	bool _lockPortal = false;
	[Export]
	Vector2 _portalId0Pos = new Vector2 (100,300);
	[Export]
	Vector2 _portalId1Pos  = new Vector2 (200,100);
	Portal PortalZero;
	Portal PortalOne;

	[Export]
	int PortalId0 = 0;
	[Export]
	int PortalId1 = 1;
		
	[Signal]
	public delegate void OnTeleport(Vector2 pos);
	
	public Portal[] Portals {get; set;}

	public override void _Ready()
	{
		
		Portals = new Portal[2] {GetNode<Portal>("Portal"),GetNode<Portal>("Portal2")}; 
		Timer = GetNode<Timer>("Timer");
		PortalZero = GetNode<Portal>("Portal");
		PortalOne = GetNode<Portal>("Portal2");   
		SetPortalStartPositions();  
		PortalZero.Id = PortalId0;
		PortalOne.Id = PortalId1;


	}

	void SetPortalStartPositions()
	{
		PortalZero.Position = _portalId0Pos;
		PortalOne.Position = _portalId1Pos;
	}
	 private void LockPortal() //prevent re-entry.
	{
		_lockPortal = true;
		Timer.Start();
	}

	private void OnTimerTimeout()
	{
		foreach (Portal portal in Portals)
		 {
			 portal.lockPortal = false;
		 }  
	}

	private void OnPortalActivated(int id)
	{
		_portalEntered = id;
		SearchForPortalLocation();
	}

	private void SearchForPortalLocation() 
	{
		foreach (Portal portal in Portals)
		{
		   // GD.Print("portal.id " + portal.id);
			portal.lockPortal = true;
			if (portal.Id != _portalEntered)
			{
				Vector2 NewPlayerPosition = new Vector2();
				NewPlayerPosition = portal.GlobalPosition;
				EmitSignal(nameof(OnTeleport), NewPlayerPosition); 
				LockPortal();
			}
		}
		return;
	}

}
