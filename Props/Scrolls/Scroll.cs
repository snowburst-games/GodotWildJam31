using Godot;
using System;
using System.Collections.Generic;

public class Scroll : Area2D
{
	[Signal]
	public delegate void SpellLearned(Spells.SpellType spell);
	private AudioStreamPlayer2D _soundPlayer;
	private Dictionary<AudioData.TreasureSound, AudioStream> _treasureSounds = AudioData.LoadedSounds<AudioData.TreasureSound>(AudioData.TreasureSoundPaths);
	private Sprite _sprite;
	private Spells.SpellType _containedSpell;
	private AnimationPlayer _anim;
	private bool _entered = false;
	public override void _Ready()
	{
		_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
		_sprite = GetNode<Sprite>("Sprite");
		_anim = GetNode<AnimationPlayer>("Anim");
	}

	public async void SetContainedSpell(Spells.SpellType spell)
	{
		_containedSpell = spell;
		switch (spell)
		{
			case Spells.SpellType.Fireball:
				_sprite.RegionRect = new Rect2(new Vector2(3810,65), new Vector2(491,411));
				break;
			case Spells.SpellType.Incinerate:
				_sprite.RegionRect = new Rect2(new Vector2(2184,65), new Vector2(491,411));
				break;
			case Spells.SpellType.Inferno:
				_sprite.RegionRect = new Rect2(new Vector2(3276,65), new Vector2(491,411));
				break;
		}
		_anim.Play("Start");	
		await ToSignal(_anim, "animation_finished");
		_anim.Play("Default");
	}
	private async void OnScrollBodyEntered(PhysicsBody2D body)
	{
		if (_entered)
		{
			return;
		}
		if (body is Unit unit)
		{
			if (unit.CurrentControlState is PlayerUnitControlState playerUnitControlState)
			{
				_entered = true;
				EmitSignal(nameof(SpellLearned), _containedSpell);
				_anim.Play("Die");
				AudioHandler.PlaySound(_soundPlayer, _treasureSounds[AudioData.TreasureSound.Scroll], AudioData.SoundBus.Effects);
				await ToSignal(_anim, "animation_finished");
				if (_soundPlayer.Playing)
				{
					await ToSignal(_soundPlayer, "finished");
				}
				QueueFree();
			}
		}
	}

}

