using Godot;
using System;
using System.Collections.Generic;

public class ManaGem : Area2D
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	// private AnimationPlayer _anim;
	private AudioStreamPlayer2D _soundPlayer;
	private Dictionary<AudioData.TreasureSound, AudioStream> _treasureSounds = AudioData.LoadedSounds<AudioData.TreasureSound>(AudioData.TreasureSoundPaths);

	private AnimationPlayer _animSingle;

	public enum GemType {Normal, Green, Red}

	public float Score {get; set;} = 1f;

	private bool _entered = false;

	[Signal]
	public delegate void GemPickedUp(float score);

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		// _anim = GetNode<AnimationPlayer>("Anim");
		_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
		_animSingle = GetNode<AnimationPlayer>("AnimSingle");
		_animSingle.Play("Start");
	}

	public void SetGemType(GemType gemType)
	{
		switch (gemType)
		{
			case GemType.Normal:
				Modulate = new Color(1,1,1);
				Score = 1f;
				break;
			case GemType.Green:
				Modulate = new Color(0.7f,1.5f,0.7f);
				Score = 5f;
				break;
			case GemType.Red:
				Modulate = new Color(2f,0.6f,0.6f);
				Score = 10f;
				break;
		}
	}

	private async void OnManaGemBodyEntered(object body)
	{
		if (_entered)
		{
			return;
		}
		if (body is Unit unit)
		{
			if (unit.CurrentControlState is PlayerUnitControlState playerUnitControlState)
			{
				_entered = true;
				playerUnitControlState.Mana += Score;
				EmitSignal(nameof(GemPickedUp), Score);
				_animSingle.Play("Die");
				AudioHandler.PlaySound(_soundPlayer, _treasureSounds[AudioData.TreasureSound.Crystal], AudioData.SoundBus.Effects);
				await ToSignal(_animSingle, "animation_finished");
				if (_soundPlayer.Playing)
				{
					await ToSignal(_soundPlayer, "finished");
				}
				QueueFree();
			}
		}
	}
}
